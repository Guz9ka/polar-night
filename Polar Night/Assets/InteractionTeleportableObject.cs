﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionTeleportableObject : MonoBehaviour
{
    [SerializeField] private GameObject interactedSlot;
    
    private GameObject _startSlot;
    private GameObject _leftHandSlot;
    private GameObject _rightHandSlot;

    private void Awake()
    {
        _startSlot = GameObject.Find(Constants.SlotInteractionResultStart);
        _leftHandSlot = GameObject.Find(Constants.SlotLeftHand);
        _rightHandSlot = GameObject.Find(Constants.SlotRightHand);
    }

    public void ToStartSlot()
    {
        transform.parent = _startSlot.transform;
        ResetTransform();
    }

    public void ToInteractedSlot()
    {
        transform.parent = interactedSlot.transform;
        ResetTransform();
    }
    
    public void ToLeftHandSlot()
    {
        Debug.Log(_leftHandSlot);
        transform.parent = _leftHandSlot.transform;
        ResetTransform();
    }
    
    public void ToRightHandSlot()
    {
        transform.parent = _rightHandSlot.transform;
        transform.localPosition = Vector3.zero;
    }

    private void ResetTransform()
    {
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
    }
}
