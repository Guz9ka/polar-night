﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Memory
{
    public static InteractionTeleportableObject LastInteractedVisual = null;
    public static Type LastInteractedType = null;
}
