﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundMaterialChecker : MonoBehaviour
{
    [SerializeField] private Transform groundChecker;
    [SerializeField] private float groundCheckDistance;

    public AudioClip GetFootstepSound()
    {
        switch (GetLayerOfGround())
        {
            case Constants.LayerSnow:
                return SoundManager.singleton.SnowWalkingSound;
            case Constants.LayerBuilding:
                return SoundManager.singleton.ConcreteWalkingSound;
            default:
                return SoundManager.singleton.ConcreteWalkingSound;
        }
    }

    private string GetLayerOfGround()
    {
        if (Physics.CheckSphere(groundChecker.position, groundCheckDistance, LayerMask.GetMask(Constants.LayerSnow)))
        {
            return Constants.LayerSnow;
        }
        else
        {
            return Constants.LayerBuilding;
        }
    }
}
