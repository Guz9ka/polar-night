﻿using UnityEngine;

public class MouseLook : MonoBehaviour
{
    [SerializeField] private float mouseSensitivity = 100.0f;
    [SerializeField] private Transform playerBody;

    private float _xRotation;
    
    private void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        
        EventManager.StartListening(Constants.EventCalledHome, DisableControl);
    }
    
    private void DisableControl()
    {
        enabled = false;
    }

    // Update is called once per frame
    private void Update()
    {
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        _xRotation -= mouseY;
        _xRotation = Mathf.Clamp(_xRotation, -90.0f, 90.0f);
        
        transform.localRotation = Quaternion.Euler(_xRotation, 0.0f, 0.0f);
        playerBody.Rotate(Vector3.up * mouseX);
    }
}
