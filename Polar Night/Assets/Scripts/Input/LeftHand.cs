﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeftHand : MonoBehaviour
{
    [SerializeField] private InteractionTeleportableObject clipboard;
    
    private Animator _animator;
    private bool _handIsFree;

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _handIsFree = true;
        clipboard.ToLeftHandSlot();
    }

    private void Update()
    {
        if (_handIsFree)
        {
            bool leftHandInput = Input.GetButton("Fire1");
            _animator.SetBool(Constants.AnimationBoolHandRaised, leftHandInput);
        }
    }
}
