﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RightHand : MonoBehaviour
{
    [SerializeField] private Transform objectSlot;
    
    private Animator _animator;
    private InteractionTeleportableObject _objectInHand;
    private bool _isHandRaised;

    void TeleportInHand()
    {
        _objectInHand = Memory.LastInteractedVisual;
        _objectInHand.ToRightHandSlot();
    }


    void TeleportOutOfHand()
    {
        if (_objectInHand != null)
        {
            _objectInHand.ToStartSlot();
            _objectInHand = null;
        }
    }

    private void Start()
    {
        _animator = GetComponent<Animator>();
        _isHandRaised = false;
        EventManager.StartListening(Constants.EventCupTaken, Take);
        EventManager.StartListening(Constants.EventKashaTaken, Take);
        EventManager.StartListening(Constants.EventSnowTaken, Take);
        EventManager.StartListening(Constants.EventSnowtreeBallTaken, Take);
        EventManager.StartListening(Constants.EventGarlandTaken, Take);
        
        EventManager.StartListening(Constants.EventWaterWarmingUp, Place);
        EventManager.StartListening(Constants.EventKashaReady, Place);
        EventManager.StartListening(Constants.EventGarlandPlaced, Place);
        
        EventManager.StartListening(Constants.EventSnowtreeBallPlaced, PlaceToTarget);
    }

    private void Take()
    {
        if (_isHandRaised)
        {
            _animator.SetTrigger(Constants.AnimationTriggerHandLower);
        }
        _animator.SetTrigger(Constants.AnimationTriggerHandRaise);
        _isHandRaised = true;
    }

    private void Place()
    {
        if (_isHandRaised)
        {
            _animator.SetTrigger(Constants.AnimationTriggerHandLower);
            _isHandRaised = false;
        }
    }
    
    private void PlaceToTarget()
    {
        if (_objectInHand != null)
        {
            _objectInHand.ToInteractedSlot();
            _objectInHand = null;
        }
        if (_isHandRaised)
        {
            _animator.SetTrigger(Constants.AnimationTriggerHandLower);
        }
        _isHandRaised = false;
    }
}
