﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseEnterDetector : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(Constants.TagPlayer))
        {
            EventManager.TriggerEvent(Constants.EventHouseEnter);
        }
    }
}
