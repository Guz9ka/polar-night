﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ClipBoard : MonoBehaviour
{
    [SerializeField] private TextMeshPro clipBoard;

    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        OnDinnerStart();

        EventManager.StartListening(Constants.EventWaterBoiled, OnWaterBoiled);
        EventManager.StartListening(Constants.EventKashaReady, OnKashaCooked);

        EventManager.StartListening(Constants.EventKashaReady, OnHeatingStart);
        EventManager.StartListening(Constants.EventBoilerTemperatureExamined, OnTemperatureExamined);
        EventManager.StartListening(Constants.EventBoilerRefueled, OnBoilerRefueled);

        EventManager.StartListening(Constants.EventBoilerRefueled, OnDecoratingStart);

        EventManager.StartListening(Constants.EventGarlandPlaced, OnChristmasTreeDecorated);
        EventManager.StartListening(Constants.EventGarlandPlaced, OnElectricityBroken);

        EventManager.StartListening(Constants.EventElectricityRepaired, OnElectricityRepaired);

        EventManager.StartListening(Constants.EventElectricityRepaired, OnCallFamilyStarted);
    }

    private void AddTask(string taskText)
    {
        clipBoard.text += $"\n{taskText}";
        PlaySound();
    }

    private void DeleteTask(string taskText)
    {
        clipBoard.text = clipBoard.text.Replace($"\n{taskText}", "");
        PlaySound();
    }

    private void PlaySound()
    {
        SoundManager.singleton.PlayInteractingSound(audioSource);
    }


    #region обед
    public void OnDinnerStart()
    {
        AddTask(Constants.TaskSetWaterBoil);
        AddTask(Constants.TaskCookKasha);
    }

    public void OnWaterBoiled()
    {
        DeleteTask(Constants.TaskSetWaterBoil);
    }

    public void OnKashaCooked()
    {
        DeleteTask(Constants.TaskSetWaterBoil);
        DeleteTask(Constants.TaskCookKasha);
    }
    #endregion

    #region отопление
    public void OnHeatingStart()
    {
        AddTask(Constants.TaskCheckTemperature);
        AddTask(Constants.TaskAddCoal);
    }

    public void OnTemperatureExamined()
    {
        DeleteTask(Constants.TaskCheckTemperature);
    }

    public void OnBoilerRefueled()
    {
        DeleteTask(Constants.TaskAddCoal);
    }
    #endregion

    #region украшение
    public void OnDecoratingStart()
    {
        AddTask(Constants.TaskDecorateChristmasTree);
    }

    public void OnChristmasTreeDecorated()
    {
        DeleteTask(Constants.TaskDecorateChristmasTree);
    }
    #endregion

    #region починка
    public void OnElectricityBroken()
    {
        AddTask(Constants.TaskRepairElectricity);
    }

    public void OnElectricityRepaired()
    {
        DeleteTask(Constants.TaskRepairElectricity);
    }
    #endregion

    #region связь с семьей
    public void OnCallFamilyStarted()
    {
        AddTask(Constants.TaskCallFamily);
    }
    #endregion
}
