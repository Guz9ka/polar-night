﻿using UnityEngine;

public class InteractableCup : InteractableBase
{
    [SerializeField] private InteractionTeleportableObject interactionTeleportableObject;
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("CUP TAKEN");
        Memory.LastInteractedVisual = interactionTeleportableObject;
        EventManager.TriggerEvent(Constants.EventCupTaken);
        Destroy(gameObject);
    }
}
