﻿using System;
using UnityEngine;

public class InteractableSnowtreeBall : InteractableBase, ITeleportable
{
    [SerializeField] private InteractionTeleportableObject snowtreeBall;
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("BALL PICKED");
        Memory.LastInteractedVisual = snowtreeBall;
        Memory.LastInteractedType = GetType();
        EventManager.TriggerEvent(Constants.EventSnowtreeBallTaken);
        Destroy(gameObject);
    }

    private void Start()
    {
        EventManager.StartListening(Constants.EventBoilerRefueled, Activate);
    }
}
