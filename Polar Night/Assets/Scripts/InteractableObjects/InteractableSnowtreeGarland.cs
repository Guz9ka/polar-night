﻿using System;
using UnityEngine;

public class InteractableSnowtreeGarland : InteractableBase, ITeleportable
{
    [SerializeField] private InteractionTeleportableObject garland;
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("GARLAND PICKED");
        Memory.LastInteractedVisual = garland;
        Memory.LastInteractedType = GetType();
        EventManager.TriggerEvent(Constants.EventGarlandTaken);
        Destroy(gameObject);
    }

    private void Start()
    {
        EventManager.StartListening(Constants.EventOutOfSnowtreeBalls, Activate);
    }
}
