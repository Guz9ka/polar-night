﻿using System;
using UnityEngine;

public class InteractableElectricBox : InteractableBase, ITeleportable
{
    [SerializeField] private InteractionTeleportableObject electricBoxBroken;
    [SerializeField] private InteractionTeleportableObject electricBoxNormal;

    private MeshRenderer _meshRenderer;

    private static int _counter;
    public override void OnInteract()
    {
        base.OnInteract();
        
        Debug.Log("LIGHTS ARE RESTORED");
        ActivateFixedBox();
        EventManager.TriggerEvent(Constants.EventElectricityRepaired);
        Deactivate(Constants.EmptyString);
        Memory.LastInteractedType = GetType();

        ++_counter;
    }
    private void ActivateBrokenBox()
    {
        electricBoxBroken.ToInteractedSlot();
        electricBoxNormal.ToStartSlot();
        Activate();
    }
    
    private void ActivateFixedBox()
    {
        electricBoxNormal.ToInteractedSlot();
        electricBoxBroken.ToStartSlot();
    }

    private void Awake()
    {
        _meshRenderer = GetComponent<MeshRenderer>();
    }

    private void Start()
    {
        ActivateFixedBox();
        EventManager.StartListening(Constants.EventGarlandPlaced, ActivateBrokenBox);
    }
}
