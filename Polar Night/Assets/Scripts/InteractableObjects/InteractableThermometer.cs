﻿using System;
using UnityEngine;

public class InteractableThermometer : InteractableBase, ITeleportable
{
    public override void OnInteract()
    {
        base.OnInteract();
        Debug.Log("THERMOMETER CHECKED");
        Memory.LastInteractedType = GetType();
        EventManager.TriggerEvent(Constants.EventBoilerTemperatureExamined);
        Destroy(this);
    }

    private void Start()
    {
        EventManager.StartListening(Constants.EventKashaReady, Activate);
    }
}
