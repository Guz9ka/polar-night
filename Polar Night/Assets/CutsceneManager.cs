﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneManager : MonoBehaviour
{
    [Header("Sound Sources")] 
    [SerializeField] private AudioSource eatingSounds;
    [SerializeField] private AudioSource shovelSounds;
    [SerializeField] private AudioSource ringSound;
    [SerializeField] private AudioSource papaSound;
    
    [Header("Cups")]
    [SerializeField] private InteractionTeleportableObject waterCup;
    [SerializeField] private InteractionTeleportableObject emptyCup;

    void Start()
    {
        EventManager.StartListening(Constants.EventKashaReady, StartEating);
        EventManager.StartListening(Constants.EventBoilerRefueled, StartRefueling);
        EventManager.StartListening(Constants.EventCalledHome, StartEnd);
    }

    private void StartEating()
    {
        Invoke(nameof(KashaAction), 1.5f);
    }

    private void StartRefueling()
    {
        Invoke(nameof(RefuelingAction), 1.5f);
    }

    private void StartEnd()
    {
        Invoke(nameof(EndAction), 1.5f);
    }

    private void KashaAction()
    {
        waterCup.ToStartSlot();
        emptyCup.ToInteractedSlot();
        SoundManager.singleton.PlayInteractingSound(eatingSounds);
    }

    private void RefuelingAction()
    {
        SoundManager.singleton.PlayInteractingSound(shovelSounds);
    }

    private void EndAction()
    {
        if (ringSound.isPlaying)
        {
            ringSound.Stop();
        }
        SoundManager.singleton.PlayInteractingSound(papaSound);
    }
}
